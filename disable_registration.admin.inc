<?php

/**
 * @file
 * Admin page callbacks for the disable_registration module.
 */

/**
 * Custom callback for admin page.
 */
function disable_registration_email_blocking() {
  $rows = array();
  $header = array(t('Blocked e-mail domains'), t('Operations'));
  $domains = db_select('blocked_email_domains', 'b')
    ->fields('b', array('eid', 'domain'))
    ->execute()
    ->fetchAllKeyed();
  foreach ($domains as $id => $domain) {
    $rows[] = array(
      $domain,
      l(t('delete'), "admin/config/people/email-blocking/delete/$id"),
    );
  }

  $output['disable_registration_email_blocking_form'] = drupal_get_form('disable_registration_email_blocking_form');

  $output['disable_registration_email_blocking_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No blocked e-mail domains available.'),
  );

  return $output;
}

/**
 * Implements hook_form().
 */
function disable_registration_email_blocking_form($form, &$form_state) {
  $form = array();

  $form['domain'] = array(
    '#title' => t('Domain'),
    '#type' => 'textfield',
    '#size' => 48,
    '#description' => t('Enter a valid e-mail domain.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#validate'][] = 'disable_registration_email_blocking_form_form_validate';
  $form['#submit'][] = 'disable_registration_email_blocking_form_submit';

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function disable_registration_email_blocking_form_form_validate($form, &$form_state) {
  $domain = trim($form_state['values']['domain']);
  if (db_query("SELECT * FROM {blocked_email_domains} WHERE domain = :domain", array(':domain' => $domain))->fetchField()) {
    form_set_error('domain', t('This e-mail domain is already blocked.'));
  }
  elseif (!valid_email_address('test@' . $domain)) {
    form_set_error('domain', t('Enter a valid e-mail domain.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function disable_registration_email_blocking_form_submit($form, &$form_state) {
  $domain = trim($form_state['values']['domain']);
  db_insert('blocked_email_domains')
    ->fields(array('domain' => $domain))
    ->execute();
  drupal_set_message(t('The e-mail domain %domain has been blocked.', array('%domain' => $domain)));
}

/**
 * Function domain deletion confirm page.
 *
 * @see disable_registration_email_blocking_delete_submit()
 */
function disable_registration_email_blocking_delete($form, &$form_state, $domain) {
  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $domain,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to delete %domain', array('%domain' => $domain['domain'])),
    'admin/config/people/email-blocking',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Process disable_registration_email_blocking_delete form submissions.
 */
function disable_registration_email_blocking_delete_submit($form, &$form_state) {
  $domain = $form_state['values']['domain'];
  db_delete('blocked_email_domains')
    ->condition('eid', $domain['eid'])
    ->execute();
  drupal_set_message(t('The e-mail address %domain was deleted.', array('%domain' => $domain['domain'])));
  $form_state['redirect'] = 'admin/config/people/email-blocking';
}
