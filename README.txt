
-- SUMMARY --

This module helps to prevent spammers registration from certain e-mail domains.
You can manage the blocked domains on the admin page "admin/config/people/email-blocking".


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see https://www.drupal.org/docs/7/extending-drupal/modules-find-import-enable-configure for further information.


-- CONFIGURATION --
  
* You can manage bloking email domains in Administration >> Configuration >> IP address blocking.


-- CONTACT --

Current maintainer:
* A.Tymchuk (WalkingDexter) - https://www.drupal.org/user/3251330

This project has been sponsored by:
* Drupal Coder
  Team of experts, who love Drupal and specialize in the development and support of
  Drupal website since 2005.

